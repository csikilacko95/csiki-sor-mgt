module.exports = {
    port: 8080,
    baseUrl: 'http://localhost:8081',
    app: {
        name: 'Gateway'
    },
    serveStatic: true,
    proxy: {
        trust: true
    },
    comment: {
        url: 'http://commentService:8080/'
    },
    auth: {
        url: 'http://localhost:3010/api'
    },
    picture: {
        url: 'http://localhost:3030/api/picture'
    }
};
