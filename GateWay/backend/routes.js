'use strict';

let request = require("request");
const config = require('../config');
const express = require('express');
const router = express.Router();

router.post('/login', (req, res) => {
    console.log('Login');
    request.post({
            headers: {
                'content-type': 'application/json'
            },
            uri: config.auth.url + '/login',
            body: req.body,
            method: 'POST',
            json: true
        },
        function (error, response, body) {
            if(response && response.statusCode === 200) {
                res.send(body);
            } else {
                res.status(401).send('Invalid email or password');
            }
        });
});

router.get('/logout', (req, res) => {
    checkToken(req, function (ok) {
        if (ok) {
            request.post({
                    headers: {
                        'content-type': 'application/json',
                        'Autentication': req.get('Authentication')
                    },
                    uri: config.auth.url + '/logout',
                    method: 'GET',
                    json: true
                },
                function (error, response, body) {
                    console.log(error, body);
                    res.send(body)
                });
        } else {
            res.status(401).send('Unauthorized!')
        }
    });
});

router.post('/register', (req, res) => {
    checkToken(req, function (ok) {
        if (!ok) {
            console.log('SendRegistration');
            request.post({
                    headers: {
                        'content-type': 'application/json'
                    },
                    uri: config.auth.url + '/auth/register',
                    method: 'POST',
                    body: req.body,
                    json: true
                },
                function (error, response, body) {
                    console.log(error, body);
                    res.send(body)
                });
        } else {
            res.status(401).send('Unauthorized!')
        }
    });
});

function checkToken(req, callback) {
    let token = req.get('x-access-token');

    if (!token) {
        callback(false);
    } else {
        request.get({
                headers: {
                    'x-access-token': req.get('Autentication'),
                    'content-type': 'application/json'
                },
                uri: config.auth.url + '/auth/check',
                method: 'POST',
                body: req.body,
                json: true
            },
            function (error, response, body) {

                if (response && response.statusCode === 200) {
                    callback(body);
                } else {
                    callback(false);
                }

            });
    }
}

router.get('/picture/:id', (req, res) => {
    checkToken(req, function (ok) {
        if (ok) {
            request(config.picture.url + "/" + req.params.id, function (error, response, body) {
                console.log(body);
                res.send(body)
            });
        } else {
            res.status(401).send('Unauthorized!')
        }
    });
});

router.get('/picture', (req, res) => {
    checkToken(req, function (ok) {
        if (ok) {
            request(config.picture.url, function (error, response, body) {
                console.log(body);
                res.send(body)
            })
        } else {
            res.status(401).send('Unauthorized!')
        }
    });
});

router.post('/picture', (req, res) => {

    checkToken(req, function (ok) {
        if (ok) {
            req.body.userid = ok.userid;
            req.body.username = ok.username;
            request.post({
                    headers: {
                        'content-type': 'application/json'
                    },
                    uri: config.picture.url,
                    method: 'POST',
                    body: req.body,
                    json: true
                },
                function (error, response, body) {
                    console.log(error, body);
                    res.send(body)
                });
        } else {
            res.status(401).send('Unauthorized!')
        }
    });
});

router.delete('/picture/:id', (req, res) => {
    checkToken(req, function (ok) {
        if (ok) {
            request.delete({
                    headers: {
                        'content-type': 'application/json'
                    },
                    uri: config.picture.url + "/" + req.params.id,
                    method: 'DELETE',
                    json: true
                },
                function (error, response, body) {
                    console.log(error, body);
                    res.send(body)
                });
        } else {
            res.status(401).send('Unauthorized!')
        }
    });
});


router.get('/comment/:id', (req, res) => {
    checkToken(req, function (ok) {
        if (ok) {
            request(config.comment.url +"/" + req.params.id, function (error, response, body) {
                console.log(body);
                res.send(body)
            })
        } else {
            res.status(401).send('Unauthorized!')
        }
    });
});

router.get('/comment', (req, res) => {
    checkToken(req, function (ok) {
        if (ok) {
            request(config.comment.url, function (error, response, body) {
                console.log(body);
                res.send(body)
            })
        } else {
            res.status(401).send('Unauthorized!')
        }
    });
});

router.post('/comment', (req, res) => {
    checkToken(req, function (ok) {
        if (ok) {
            req.body.userid = ok.userid;
            req.body.username = ok.username;
            request.post({
                    headers: {
                        'content-type': 'application/json'
                    },
                    uri: config.picture.url,
                    method: 'POST',
                    body: req.body,
                    json: true
                },
                function (error, response, body) {
                    console.log(error, body);
                    res.send(body)
                });
        } else {
            res.status(401).send('Unauthorized!')
        }
    });
});

router.delete('/picture/:id', (req, res) => {
    checkToken(req, function (ok) {
        if (ok) {

            request.delete({
                    headers: {
                        'content-type': 'application/json'
                    },
                    uri: config.comment.url + "/" + req.params.id,
                    method: 'DELETE',
                    json: true
                },
                function (error, response, body) {
                    console.log(error, body);
                    res.send(body)
                });
        } else {
            res.status(401).send('Unauthorized!')
        }
    });
});

module.exports = router;
