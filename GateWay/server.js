// load express module
let express = require('express');
//load path module
//



// load different middleware to handle teh reqs
//
let bodyParser = require ('body-parser');


// set the host name
let hostname 	= 'localhost';
//set the port nr
let port 	= 3000;



// load the predefined routes


const config = require('./config');


// create the general app context

let app		= express();

// start the middleware
app.set('config', config);



app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));



app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', 'http://localhost:8080');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    res.header('Access-Control-Allow-Credentials', 'true');
    next()
});

app.get('/', function (req, res) {
    res.status(200).send('API works.');
});

global.__root   = __dirname + '/';
let routerController = require(__root + 'backend/routes');

app.use('/api', routerController);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function(err, req, res) {

    // render the error page
    res.status(err.status || 500);

    res.json({
        message: err.message,
        error: {}
    });
});



// start the app
app.listen( port, hostname, function () {
    console.log(`Server running at http://${hostname}:${port}/`);
});
