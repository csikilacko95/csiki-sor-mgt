let express = require('express');
let app = express();

const mongoose = require('mongoose');
mongoose.connect('mongodb://root:test123@ds161520.mlab.com:61520/mgt-userdb');
// create the connection handler
let db = mongoose.connection;

// register the callbacks for different events like on or once
db.on('error', function () {
    console.error.bind(console, 'connection error:');
});

db.once('open', function () {
    console.log('Connected correctly to the DB server ');
});

global.__root   = __dirname + '/';

app.get('/api', function (req, res) {
    res.status(200).send('API works.');
});

let UserController = require(__root + 'controllers/userController');
app.use('/api/users', UserController);

let AuthController = require(__root + 'controllers/authController');
app.use('/api/auth', AuthController);

module.exports = app;