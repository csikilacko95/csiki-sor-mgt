module.exports = {
    port: 3000,
    baseUrl: 'http://localhost',
    app: {
        name: 'AuthService'
    },
    serveStatic: true,
    proxy: {
        trust: true
    },
    mysql: {
        host     : 'localhost',
        user     : 'root',
        password : 'root',
        database : 'UserDB',
        name     : 'mysql'
    },
    secretKey: 'supersecret'
};
