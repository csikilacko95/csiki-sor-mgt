const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');

router.use(bodyParser.urlencoded({extended: true}));
router.use(bodyParser.json());
const Comment = require('../models/comment');

// CREATES A NEW COMMENT
router.post('/', function (req, res) {
    Comment.create({
                text: req.body.text,
                date: req.body.date,
                device: req.body.device,
                userid: req.body.userid,
                username: req.body.username,
                pictureid: req.body.pictureid,
                picturename: req.body.picturename
        },
        function (err, comment) {
            if (err) return res.status(500).send("There was a problem adding the information to the database.");
            res.status(200).send(comment);
        });
});

// RETURNS ALL THE COMMENTS IN THE DATABASE
router.get('/', function (req, res) {
    Comment.find({}, function (err, users) {
        if (err) return res.status(500).send("There was a problem finding the comments.");
        res.status(200).send(users);
    });
});

// GETS A SINGLE COMMENT FROM THE DATABASE
router.get('/:id', function (req, res) {
    Comment.findById(req.params.id, function (err, user) {
        if (err) return res.status(500).send("There was a problem finding the comment.");
        if (!user) return res.status(404).send("No user found.");
        res.status(200).send(user);
    });
});

// DELETES A COMMENT FROM THE DATABASE
router.delete('/:id', function (req, res) {
    Comment.findByIdAndRemove(req.params.id, function (err, comment) {
        if (err) return res.status(500).send("There was a problem deleting the comment.");
        res.status(200).send("Comment: " + comment.text + " was deleted.");
    });
});

module.exports = router;

