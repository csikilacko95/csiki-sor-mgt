module.exports = {
    port: 3030,
    baseUrl: 'http://localhost',
    app: {
        name: 'CommentService'
    },
    serveStatic: true,
    proxy: {
        trust: true
    }
    secretKey: 'supersecret'
};
