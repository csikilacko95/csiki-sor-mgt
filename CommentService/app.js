let express = require('express');
let app = express();

const mongoose = require('mongoose');
mongoose.connect('mongodb://commentuser:comment4password@ds261540.mlab.com:61540/picture_comment');
// create the connection handler
let db = mongoose.connection;

// register the callbacks for different events like on or once
db.on('error', function () {
    console.error.bind(console, 'connection error:');
});

db.once('open', function () {
    console.log('Connected correctly to the DB server ');
});

global.__root   = __dirname + '/';

app.get('/api', function (req, res) {
    res.status(200).send('API works.');
});

let CommentController = require(__root + 'controllers/commentController');
app.use('/api/comment', CommentController);

module.exports = app;
