let mongoose = require('mongoose');
let CommentSchema = new mongoose.Schema({
    text: String,
    date: String,
    device: String,
    userid: Number,
    username: String,
    pictureid: Number,
    picturename: String
});
mongoose.model('Comment', CommentSchema);

module.exports = mongoose.model('Comment');
