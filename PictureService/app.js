let express = require('express');
let app = express();

const mongoose = require('mongoose');
mongoose.connect('mongodb://root:root2018@ds161610.mlab.com:61610/picture-service');
// create the connection handler
let db = mongoose.connection;

// register the callbacks for different events like on or once
db.on('error', function () {
    console.error.bind(console, 'connection error:');
});

db.once('open', function () {
    console.log('Connected correctly to the DB server ');
});

global.__root   = __dirname + '/';

app.get('/api', function (req, res) {
    res.status(200).send('API works.');
});

let PictureController = require(__root + 'controllers/pictureController');
app.use('/api/picture', PictureController);

module.exports = app;
