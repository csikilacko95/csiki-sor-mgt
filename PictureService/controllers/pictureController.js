const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');

router.use(bodyParser.urlencoded({extended: true}));
router.use(bodyParser.json());
const Picture  = require('../models/picture');

// CREATES A NEW COMMENT
router.post('/', function (req, res) {
    Picture.create({
            title: req.body.title,
            device: req.body.device,
            src: req.body.src,
            date: req.body.date,
            userid: req.body.userid,
            username: req.body.username,
        },
        function (err, comment) {
            if (err) return res.status(500).send("There was a problem adding the information to the database.");
            res.status(200).send(comment);
        });
});

// RETURNS ALL THE COMMENTS IN THE DATABASE
router.get('/', function (req, res) {
    Picture.find({}, function (err, pictures) {
        if (err) return res.status(500).send("There was a problem finding the comments.");
        res.status(200).send(pictures);
    });
});

// GETS A SINGLE COMMENT FROM THE DATABASE
router.get('/:id', function (req, res) {
    Picture.findById(req.params.id, function (err, picture) {
        if (err) return res.status(500).send("There was a problem finding the comment.");
        if (!picture) return res.status(404).send("No picture found.");
        res.status(200).send(picture);
    });
});

// DELETES A COMMENT FROM THE DATABASE
router.delete('/:id', function (req, res) {
    Picture.findByIdAndRemove(req.params.id, function (err, picture) {
        if (err) return res.status(500).send("There was a problem deleting the comment.");
        res.status(200).send("Comment: " + picture.title + " was deleted.");
    });
});

module.exports = router;

