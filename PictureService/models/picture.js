let mongoose = require('mongoose');
let PictureSchema = new mongoose.Schema({
    title: String,
    device: String,
    src: String,
    date: Date,
    userid: Number,
    username: String,
});
mongoose.model('Picture', PictureSchema);

module.exports = mongoose.model('Picture');
