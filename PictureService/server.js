let app = require('./app');
let port = process.env.PORT || 3030;

app.listen(port, function() {
    console.log('Picture service listening on port: ' + port);
});
