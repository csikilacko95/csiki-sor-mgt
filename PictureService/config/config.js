module.exports = {
    port: 3030,
    baseUrl: 'http://localhost',
    app: {
        name: 'PictureService'
    },
    serveStatic: true,
    proxy: {
        trust: true
    },
    secretKey: 'supersecret'
};
